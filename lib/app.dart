import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled/pages/intro/splash_page.dart';
import 'package:untitled/providers/intro/splash_state.dart';
import 'package:untitled/states/app_state.dart';
import 'package:untitled/theme.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AppState(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Company',
        theme: appTheme,
        initialRoute: '/',
        routes: {
          SplashPage.routeName: (context) {
            return ChangeNotifierProvider(
              create: (context) => SplashState(),
              child: SplashPage(),
            );
          },
        },
      ),
    );
  }
}
