import 'package:flutter/material.dart';

double deviceWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double deviceHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

// Future<String> getVersion() async {
//   final PackageInfo packageInfo = await PackageInfo.fromPlatform();
//   return packageInfo.version;
// }