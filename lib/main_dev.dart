import 'package:flutter/material.dart';
import 'package:untitled/app.dart';
import 'package:untitled/data/api.dart';
import 'package:untitled/data/cache.dart';
import 'package:untitled/data/config.dart';
import 'package:untitled/data/constant.dart';

Future<void> main() async {
  await initTokenDatabase();

  FlavorConfig(
    flavor: Flavor.DEV,
    values: FlavorValues(
      baseUrl: BASE_URL_DEV,
    ),
  );
  ApiClient(values: FlavorConfig.instance.values);
  runApp(MyApp());
}
