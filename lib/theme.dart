import 'package:flutter/material.dart';

//Colors
const primaryColor = Color(0xFFE35334);
const accentColor = Color(0xFFE35334);
const errorColor = Color(0xFFFF1010);
const errorColor2 = Color(0xFFAA1F1F);
const errorBackgroundColor = Color(0xFFFFDFDF);
const errorBackgroundColor2 = Color(0xFFFFD3D3);
const homeBackgroundColor = Color(0xFFF7F9FF);
const dashedBorder = Color(0xFFE5E5E5);
const unselectedColor = Color(0xFF979797);
const blackFive = Color(0xFF555555);
const blackSeven = Color(0xFF777777);
const blackHolder = Color(0xFFC4C4C4);
const blackTitle = Color(0xFF1A1A1A);
const black = Color(0xFF000000);
const textColor = Color(0xFF4A4A4A);
const divider = Color(0xFFE7EAEE);
const green = Color(0xFF0E903B);
const blue2 = Color(0xFF2D9CDB);
const gradientSuccess1 = Color(0xFF18BF89);
const gradientSuccess2 = Color(0xFF62CF72);
const gradientWarning1 = Color(0xFFD77D15);
const gradientWarning2 = Color(0xFFF9A424);
const gradientFailed1 = Color(0xFFAF2424);
const gradientFailed2 = Color(0xFFEC5858);
const backgroundLoginColor = Color(0xFFFFFAFA);
const favoriteColor = Color(0xFFFDB62F);
const sapphireColor = Color(0xFF001F5C);

const appBarTheme = AppBarTheme(
  color: Colors.white,
);

final appTheme = ThemeData(
  backgroundColor: Colors.white,
  primaryColor: primaryColor,
  accentColor: accentColor,
  appBarTheme: appBarTheme,
  primaryTextTheme: const TextTheme(
    headline6: TextStyle(
      color: Colors.black,
      fontSize: 16.0,
      fontWeight: FontWeight.bold,
    ),
  ),

  // Define the default Font Family
  fontFamily: 'Nunito Sans',
);
