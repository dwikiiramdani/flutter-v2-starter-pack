import 'package:flutter/material.dart';

enum Flavor { DEV, PROD }

class FlavorValues {
  FlavorValues({
    @required this.baseUrl,
    // @required this.apiKey,
    // @required this.gChannel,
  });
  final String baseUrl;
  // final String apiKey;
  // final int gChannel;
}

class FlavorConfig {
  final Flavor flavor;
  final FlavorValues values;
  final String name;
  static FlavorConfig _instance;

  factory FlavorConfig({
    @required Flavor flavor,
    @required FlavorValues values,
  }) {
    _instance ??= FlavorConfig._internal(flavor, values, flavor.toString());
    return _instance;
  }

  FlavorConfig._internal(this.flavor, this.values, this.name);
  static FlavorConfig get instance => _instance;

  static bool isProduction() => _instance.flavor == Flavor.PROD;
  static bool isDevelopment() => _instance.flavor == Flavor.DEV;
}
