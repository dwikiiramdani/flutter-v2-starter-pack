import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

const PREF_TOKEN_BOX = 'pref_token_box';
const PREF_LOCAL_BOX = 'pref_local_box';
Box tokenBox;
Box localBox;
Future<void> initTokenDatabase() async {
  await Hive.initFlutter();
  tokenBox = await Hive.openBox<void>(PREF_TOKEN_BOX);
  localBox = await Hive.openBox<void>(PREF_LOCAL_BOX);
}

const PREF_TOKEN_USER = 'pref_user_token';
Future<String> getToken() async {
  final String userToken =
  await tokenBox.get(PREF_TOKEN_USER, defaultValue: '');
  return userToken;
}

Future<void> saveToken(String token) async {
  if (tokenBox != null) {
    await tokenBox.put(PREF_TOKEN_USER, token);
  }
}

Future<void> removeToken() async {
  if (tokenBox != null) {
    await tokenBox.delete(PREF_TOKEN_USER);
  }
}

const PREF_LOCALIZATION = 'pref_localization';
Future<String> getLocalization() async {
  final String localization =
  await localBox.get(PREF_LOCALIZATION, defaultValue: '1');
  return localization;
}

Future<void> saveLocalization(String localization) async {
  if (localBox != null) {
    await localBox.put(PREF_LOCALIZATION, localization);
  }
}

Future<void> removeLocalization() async {
  if (localBox != null) {
    await localBox.delete(PREF_LOCALIZATION);
  }
}
