import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:quiver/strings.dart';
import 'package:untitled/data/config.dart';

class GemException implements Exception {
  GemException(this.message, {this.uri});
  final String message;
  final Uri uri;

  String toString() {
    var b = new StringBuffer()..write('GemException: ')..write(message);
    if (uri != null) {
      b.write(', uri = $uri');
    }
    return b.toString();
  }
}

class ApiClient {
  static const int TIMEOUT_SECONDS = 30;
  final String baseUrl;
  // final String apiKey;
  // final int gChannel;
  String authorization;
  String version;
  static ApiClient _instance;

  factory ApiClient({
    @required FlavorValues values,
  }) {
    _instance ??= ApiClient._internal(values.baseUrl);
    // ApiClient._internal(values.baseUrl, values.apiKey, values.gChannel);
    return _instance;
  }

  ApiClient._internal(this.baseUrl /*this.apiKey, this.gChannel*/);
  static ApiClient get instance => _instance;
  static void setAuth(String auth) {
    _instance.authorization = auth;
  }

  static void setVersion(String version) {
    _instance.version = version;
  }

  @visibleForTesting
  static void setInstance(ApiClient apiClient) => _instance = apiClient;

  Map<String, String> getHeaders() {
    Map<String, String> header = {
      // "G-Channel": gChannel.toString(),
      // "Api-Key": apiKey, // or whatever
      "Content-Type": "application/json",
      "Accept": "application/json",
    };
    if (isNotEmpty(authorization)) {
      header.addAll({"Authorization": authorization});
    }
    return header;
  }

  Future<String> getData(String endpoint, String data) async {
    final String url = baseUrl + endpoint + data;
    try {
      final response = await http
          .get(Uri.parse(url), headers: getHeaders())
          .timeout(Duration(seconds: TIMEOUT_SECONDS));
      if (response.statusCode != 200) {
        throw GemException(
          'Invalid response ${response.statusCode}',
          uri: Uri.parse(url),
        );
      }
      return response.body;
    } on Exception catch (e) {
      if (e is TimeoutException) {
        throw GemException(
          'Server Timeout',
          uri: Uri.parse(url),
        );
      } else if (e is SocketException) {
        throw GemException(
          e.message != null && e.message.isNotEmpty
              ? e.message.contains('host lookup')
                  ? 'Koneksi internet mati. Silahkan periksa jaringan internet device.'
                  : e.message
              : e.osError.message,
          uri: Uri.parse(url),
        );
      }
      throw e;
    }
  }

  Future<String> postData(String endpoint, dynamic data) async {
    final String url = baseUrl + endpoint;
    try {
      final response = await http
          .post(Uri.parse(url), headers: getHeaders(), body: json.encode(data))
          .timeout(Duration(seconds: TIMEOUT_SECONDS));
      if (response.statusCode != 200) {
        throw GemException(
          'Invalid response ${response.statusCode}',
          uri: Uri.parse(url),
        );
      }
      return response.body;
    } on Exception catch (e) {
      if (e is TimeoutException) {
        throw GemException(
          'Server Timeout',
          uri: Uri.parse(url),
        );
      } else if (e is SocketException) {
        throw GemException(
          e.message != null && e.message.isNotEmpty
              ? e.message.contains('host lookup')
                  ? 'Koneksi internet mati. Silahkan periksa jaringan internet device.'
                  : e.message
              : e.osError.message,
          uri: Uri.parse(url),
        );
      }
      throw e;
    }
  }

  Future<String> putData(String endpoint, dynamic data) async {
    final String url = baseUrl + endpoint;
    try {
      final response = await http
          .put(Uri.parse(url), headers: getHeaders(), body: json.encode(data))
          .timeout(Duration(seconds: TIMEOUT_SECONDS));
      if (response.statusCode != 200) {
        throw GemException(
          'Invalid response ${response.statusCode}',
          uri: Uri.parse(url),
        );
      }
      return response.body;
    } on Exception catch (e) {
      if (e is TimeoutException) {
        throw GemException(
          'Server Timeout',
          uri: Uri.parse(url),
        );
      } else if (e is SocketException) {
        throw GemException(
          e.message != null && e.message.isNotEmpty
              ? e.message.contains('host lookup')
                  ? 'Koneksi internet mati. Silahkan periksa jaringan internet device.'
                  : e.message
              : e.osError.message,
          uri: Uri.parse(url),
        );
      }
      throw e;
    }
  }
}
