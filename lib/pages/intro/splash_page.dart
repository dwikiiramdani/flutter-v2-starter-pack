import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled/helper/util.dart';
import 'package:untitled/providers/intro/splash_state.dart';
import 'package:untitled/theme.dart';

class SplashPage extends StatefulWidget {
  static const routeName = '/';

  const SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  SplashState splashState;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      splashState.setToken();
    });
  }

  @override
  Widget build(BuildContext context) {
    final double widthItem = deviceWidth(context) - 80;
    final double heightItem = deviceHeight(context) - 80;

    splashState = Provider.of<SplashState>(context);
    splashState.setContext(context);
    return Scaffold(
      backgroundColor: homeBackgroundColor,
      body: Center(
        child: Image(
          image: const AssetImage('assets/images/logo.png'),
          width: widthItem,
          height: heightItem,
        ),
      ),
    );
  }
}
