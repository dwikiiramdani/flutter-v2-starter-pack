import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled/providers/content/home_state.dart';

class HomePage extends StatefulWidget {
  static const routeName = '/home';

  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin<HomePage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  HomeState state;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      print('HomePage');
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    state = Provider.of<HomeState>(context);
    state.setContextAndKey(context, _scaffoldKey);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(child: Text('HomePage')),
    );
  }
}
