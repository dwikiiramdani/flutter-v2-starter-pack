import 'package:flutter/material.dart';

class HomeState with ChangeNotifier {
  BuildContext context;
  GlobalKey<ScaffoldState> key;
  void setContextAndKey(BuildContext ctx, GlobalKey<ScaffoldState> key) {
    context = ctx;
    this.key = key;
  }
}