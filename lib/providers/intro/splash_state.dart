import 'package:flutter/material.dart';
import 'package:untitled/data/api.dart';
import 'package:untitled/data/cache.dart';

class SplashState with ChangeNotifier {
  BuildContext context;

  void setContext(BuildContext ctx) {
    context = ctx;
  }

  String version = '';
  String token = '';

  Future<void> setToken() async {
    // version = await getVersion();
    token = await getToken();
   ApiClient.setAuth(token);
//     ApiClient.setVersion(version);
    notifyListeners();
  }

  bool isFetching = false;

  void setFetching(bool isFetching) {
    this.isFetching = isFetching;
    notifyListeners();
  }
}
